= HAM modeling with vocabularies
:toc: preamble
:toclevels: 3
:schema-base: https://schema.org/
:rfc-base: http://tools.ietf.org/html/
:ham: link:README.adoc

== Intro

This is a simple example to model e.g. {schema-base}[schema.org] vocabularies with {ham}[HAM].
For demonstration I use a {schema-base}Conversation[Conversation] which cointains also {schema-base}Message[Messages].

== Modeling

A Conversation of our project contains a reduced profile of the Conversation.
We support only the following properties:

* {schema-base}author[author]
* {schema-base}dateCreated[dateCreated]
* {schema-base}hasPart[hasPart]
* {schema-base}identifier[identifier]


.Conversation with inline author
[source,json]
----
{
  "ham" :
  {
    "schema": "Draft-3",
    "items" [
      {
        "links": [
          {"rel": "type", "href": "https://schema.org/Conversation", "prompt": "Conversation"}
        ],
        "entries": [
          {"name": "dateCreated", "value": "20220624T13:55:16Z", "prompt": "Creation date"},
          {"name": "identifier", "value": "f3d3d903-84fe-40a6-9afe-43f94ad4ee52", "prompt": "Reference"},
          {"name": "author", "prompt": "Author", "entries":
            [
              {"name": "name", "value": "Hans Hypochonder", "prompt": "Name"},
              {"name": "givenName", "value": "Hans", "prompt": "Vorname"},
              {"name": "familyName", "value": "Hypochonder", "prompt": "Nachname"},
              {"name": "honorificPrefix", "value": "Dr", "prompt": "Title"},
              {"name": "honorificSuffix", "value": "MD", "prompt": "Title"}
            ]
          }
        ]
      }
    ]
  }
}
----

The problem with the above example is the `author` inline part.
The {schema-base}Conversation[Conversation] {schema-base}author[author] property can be of {schema-base}Organization[Organization] or {schema-base}Person[Person].
A client must now interprete the properties and decide, based on these properties, of which type the `author` is.
But what if we deliver only the `name` property?
`name` comes from {schema-base}Thing[Thing] and is part of every type.
So it is also part of {schema-base}Organization[Organization] and {schema-base}Person[Person].

A better solution is to have an own {ham}[HAM] item for the {schema-base}Person[Person] type.

.Conversation with separate author
[source,json]
----
{
  "ham" :
  {
    "schema": "Draft-3",
    "items" [
      {
        "links": [
          {"rel": "type", "href": "https://schema.org/Conversation", "prompt": "Conversation"}
        ],
        "entries": [
          {"name": "dateCreated", "value": "20220624T13:55:16Z", "prompt": "Creation date"},
          {"name": "identifier", "value": "f3d3d903-84fe-40a6-9afe-43f94ad4ee52", "prompt": "Reference"}
        ]
      },

      {
        "links": [
          {"rel": "type", "href": "https://schema.org/Person", "prompt": "Person"}
        ],
        "entries": [
          {"name": "name", "value": "Hans Hypochonder", "prompt": "Name"}
        ]
      }
    ]
  }
}
----

If we've only one Conversation in our HAM document this may be fine.
But what if we've multiple Conversations of different authors?
In such a case there must be a link between the Conversation and the Person.

{ham}[HAM] offers the `id` property as solution.

An `id` can be a artifical machine readble value which must be unique in the {ham}[HAM] document. So we add the `"id": "zwer87rwkjh"` property to the {schema-base}Person[Person] item.
The {schema-base}Conversation[Conversation] item becomes a link relation which contains only a URI fragment ({rfc-base}rfc8820[RFC 8820], section 2.5)


.Conversation with separate author and a link relation between
[source,json]
----
{
  "ham" :
  {
    "schema": "Draft-3",
    "items" [
      {
        "links": [
          {"rel": "type", "href": "https://schema.org/Conversation", "prompt": "Conversation"},
          {"rel": "author", "href": "#zwer87rwkjh", "prompt": "Author"}
        ],
        "entries": [
          {"name": "dateCreated", "value": "20220624T13:55:16Z", "prompt": "Creation date"},
          {"name": "identifier", "value": "f3d3d903-84fe-40a6-9afe-43f94ad4ee52", "prompt": "Reference"}
        ]
      },

      {
        "id": "zwer87rwkjh",
        "links": [
          {"rel": "type", "href": "https://schema.org/Person", "prompt": "Person"}
        ],
        "entries": [
          {"name": "name", "value": "Hans Hypochonder", "prompt": "Name"}
        ]
      }
    ]
  }
}
----

=== Messages

A {schema-base}Conversation[Conversation] may contain {schema-base}Message[Messages] between different {schema-base}Person[Persons].
The Conversation type defines itself as 

[quote]
One or more messages between organizations or people on a particular topic. Individual messages can be linked to the conversation with isPartOf or hasPart properties.
 
Two possibles solutions are possible:

1. link relation to an endpoint of the {rfc-base}rfc6573[collection] of messages
2. Inline the messages

This howto handles only the inline of messages.
The link relation should be straight forward.


.Conversation with multiple messages of different authors
[source,json]
----
{
  "ham" :
  {
    "schema": "Draft-3",
    "items" [
      {
        "id": "eoi387d",
        "links": [
          {"rel": "type", "href": "https://schema.org/Conversation", "prompt": "Conversation"},
          {"rel": "author", "href": "#zwer87rwkjh", "prompt": "Author"},
          {"rel": "hasPart", "href": "#f1stm", "prompt": "First message"},
        ],
        "entries": [
          {"name": "dateCreated", "value": "20220624T13:55:16Z", "prompt": "Creation date"},
          {"name": "identifier", "value": "f3d3d903-84fe-40a6-9afe-43f94ad4ee52", "prompt": "Reference"}
        ]
      },

      {
        "id": "zwer87rwkjh",
        "links": [
          {"rel": "type", "href": "https://schema.org/Person", "prompt": "Person"}
        ],
        "entries": [
          {"name": "name", "value": "Hans Hypochonder", "prompt": "Name"}
        ]
      },
      
      {
        "id": "f1stm",
        "links": [
          {"rel": "type", "href": "https://schema.org/Message", "prompt": "Message"},
          {"rel": "isPartOf", "href": "#eoi387d"},
          {"rel": "sender", "href": "#zwer87rwkjh", "prompt": "Author"},
          {"rel": "next", "href": "#euf2ndm", "prompt": "Next Message"}
        ],
        "entries": [
          {"name": "text", "value": "Lorem ipsum dolor sit amet, consectetur adipisici elit, …", "prompt": "Message text"},
          {"name": "dateCreated", "value": "20220624T13:55:16Z", "prompt": "Send at"},
          {"name": "encodingFormat", "value": "text/plain", "prompt": "Message format"}
        ]
      },

      {
        "id": "euf2ndm",
        "links": [
          {"rel": "type", "href": "https://schema.org/Message", "prompt": "Message"},
          {"rel": "isPartOf", "href": "#eoi387d"},
          {"rel": "sender", "href": "#chfdhjdj", "prompt": "Author"},
          {"rel": "prev", "href": "#f1stm", "prompt": "Previous Message"}
        ],
        "entries": [
          {"name": "text", "value": "Neque porro quisquam est, qui dolorem ipsum, quia dolor sit …", "prompt": "Message text"},
          {"name": "dateCreated", "value": "20220625T23:12:16Z", "prompt": "Send at"},
          {"name": "encodingFormat", "value": "text/plain", "prompt": "Message format"}
        ]
      },

      {
        "id": "chfdhjdj",
        "links": [
          {"rel": "type", "href": "https://schema.org/Person", "prompt": "Person"}
        ],
        "entries": [
          {"name": "name", "value": "John Doe", "prompt": "Name"},
          {"name": "identifier", "value": "vbu2750", "prompt": "Reference"}
        ]
      }
    ]
  }
}
----
